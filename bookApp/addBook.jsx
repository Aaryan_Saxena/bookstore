import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./bookServer";
class AddBooks extends Component {
  state = {
    book: {
      name: "",
      author: "",
      description: "",
      blurb: "",
      review: "",
      price: "",
      year: "",
      publisher: "",
      avgrating: "",
      genre: "",
      language: "",
      bestseller: "",
      newarrival: "",
    },
    genres: ["Fiction", "Children", "Mystery", "Management"],
    languages: ["Latin", "English", "French", "Others"],
    bestSeller: ["Yes", "No"],
    newArrival: ["Yes", "No"],
    errors: {},
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.book[input.name] = input.value;
    this.setState(s1);
  };
  async postData(url, obj) {
    let response = await http.post(url, obj);
    this.props.history.push("/bookS");
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors(this.state.book);
    console.log(errors);
    if (this.isvalid(errors)) this.postData("/booksapp/book", this.state.book);
    else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = (book) => {
    let {
      name,
      author,
      description,
      blurb,
      review,
      price,
      year,
      publisher,
      avgrating,
      genre,
      language,
      bestseller,
      newarrival,
    } = book;
    let json = {};
    json.name = this.handleValidateName(name);
    json.author = this.handleValidateAuthor(author);
    json.description = this.handleValidateDesc(description);
    json.blurb = this.handleValidateBlurb(blurb);
    json.review = this.handleValidateReview(review);
    json.price = this.handleValidatePrice(price);
    json.year = this.handleValidateYear(year);
    json.publisher = this.handleValidatePublisher(publisher);
    json.avgrating = this.handleValidateRating(avgrating);
    json.genre = this.handleValidateGenre(genre);
    json.language = this.handleValidateLang(language);
    json.bestseller = this.handleValidateBestSeller(bestseller);
    json.newarrival = this.handleValidateNewArrival(newarrival);
    return json;
  };
  handleValidateName = (name) => (!name ? "Book name is mandatory" : "");
  handleValidateAuthor = (author) => (!author ? "Author is mandatory" : "");
  handleValidateDesc = (description) =>
    !description ? "Description is mandatory" : "";
  handleValidateBlurb = (blurb) => (!blurb ? "Blurb is mandatory" : "");
  handleValidateReview = (review) => (!review ? "Review is mandatory" : "");
  handleValidatePrice = (price) => (!price ? "Price is mandatory" : "");
  handleValidateYear = (year) => (!year ? "Year is mandatory" : "");
  handleValidatePublisher = (publisher) =>
    !publisher ? "Publisher is mandatory" : "";
  handleValidateRating = (avgrating) =>
    !avgrating ? "Rating is mandatory" : "";
  handleValidateGenre = (genre) => (!genre ? "Select Genre" : "");
  handleValidateLang = (language) => (!language ? "Select Language" : "");
  handleValidateBestSeller = (bestseller) =>
    !bestseller ? "Choose Applicable Option" : "";
  handleValidateNewArrival = (newarrival) =>
    !newarrival ? "Choose Applicable Option" : "";
  handleValidate = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    let {
      name,
      author,
      description,
      blurb,
      review,
      price,
      year,
      publisher,
      avgrating,
      genre,
      language,
      bestseller,
      newarrival,
    } = s1.book;
    switch (input.name) {
      case "name":
        s1.errors.name = this.handleValidateName(name);
        break;
      case "author":
        s1.errors.author = this.handleValidateAuthor(author);
        break;
      case "description":
        s1.errors.description = this.handleValidateDesc(description);
        break;
      case "blurb":
        s1.errors.blurb = this.handleValidateBlurb(blurb);
        break;
      case "review":
        s1.errors.review = this.handleValidateReview(review);
        break;
      case "price":
        s1.errors.price = this.handleValidatePrice(price);
        break;
      case "year":
        s1.errors.year = this.handleValidateYear(year);
        break;
      case "publisher":
        s1.errors.publisher = this.handleValidatePublisher(publisher);
        break;
      case "avgrating":
        s1.errors.avgrating = this.handleValidateRating(avgrating);
        break;
      case "genre":
        s1.errors.genre = this.handleValidateGenre(genre);
        break;
      case "language":
        s1.errors.language = this.handleValidateLang(language);
        break;
      default:
        break;
    }
    this.setState(s1);
  };
  render() {
    let {
      name,
      author,
      description,
      blurb,
      review,
      price,
      year,
      publisher,
      avgrating,
      genre,
      language,
      bestseller,
      newarrival,
    } = this.state.book;
    let { languages, genres, errors = {}, bestSeller, newArrival } = this.state;
    return (
      <div className="container">
        <div className="row mt-3">
          <div className="col-9 offset-3 text-center">
            <h4>Create a New Book</h4>
          </div>
        </div>
        {this.makeTextField("name", name, "Name", errors.name)}
        {this.makeTextField("author", author, "Author", errors.author)}
        {this.makeTextField(
          "description",
          description,
          "Description",
          errors.description
        )}
        {this.makeTextField("blurb", blurb, "Blurb", errors.blurb)}
        {this.makeTextField("review", review, "Review", errors.review)}
        {this.makeTextField("price", price, "Price", errors.price)}
        {this.makeTextField("year", year, "Year", errors.year)}
        {this.makeTextField(
          "publisher",
          publisher,
          "Publisher",
          errors.publisher
        )}
        {this.makeTextField(
          "avgrating",
          avgrating,
          "Avgrating",
          errors.avgrating
        )}
        {this.makeDD(
          genres,
          genre,
          "genre",
          "Genre",
          "Select Genre",
          errors.genre
        )}
        {this.makeDD(
          languages,
          language,
          "language",
          "Language",
          "Select Language",
          errors.language
        )}
        {this.makeRadio(bestSeller, bestseller, "bestseller", "BestSeller")}
        {this.makeRadio(newArrival, newarrival, "newarrival", "NewArrival")}
        <div className="col-9 offset-3 text-center">
          <button
            className="btn btn-primary m-2"
            disabled={!this.isFormValid()}
            onClick={this.handleSubmit}
          >
            Create Book
          </button>
        </div>
      </div>
    );
  }
  isFormValid = () => {
    let errors = this.checkErrors(this.state.book);
    return this.isvalid(errors);
  };
  makeTextField = (name, value, label, errors) => {
    return (
      <div className="row">
        <div className="col-3">
          <label>{label}</label>
        </div>
        <div className="col-9">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name={name}
              value={value}
              onChange={this.handleChange}
              onBlur={this.handleValidate}
            />
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </div>
    );
  };
  makeDD = (arr, value, name, label, valOnTop, errors) => {
    return (
      <div className="row">
        <div className="col-3">
          <label>{label}</label>
        </div>
        <div className="col-9">
          <div className="form-group">
            <select
              className="form-control"
              name={name}
              value={value}
              onChange={this.handleChange}
              onBlur={this.handleValidate}
            >
              <option value="">{valOnTop}</option>
              {arr.map((opt) => (
                <option key={opt}>{opt}</option>
              ))}
            </select>
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </div>
    );
  };
  makeRadio = (arr, value, name, label) => {
    return (
      <div className="row mt-2">
        <div className="col-3">
          <label className="form-check-label">{label}</label>
        </div>
        <div className="col-9">
          <div className="row">
            {arr.map((opt) => (
              <div className="col-6" key={opt}>
                <div className="form-check" key={opt}>
                  <input
                    type="radio"
                    className="form-check-input mx-2"
                    value={opt}
                    name={name}
                    checked={value === opt}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label mx-4">{opt}</label>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  };
}
export default AddBooks;
