import React, { Component } from "react";
import { Link } from "react-router-dom";
class BookNavbar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link to="/books" className="navbar-brand">
          BookSite
        </Link>
        <div id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to="/books?newarrival=yes" className="nav-link">
                New Arrivals
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/books/Children" className="nav-link">
                Children
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/books/Fiction" className="nav-link">
                Fiction
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/books/Mystery" className="nav-link">
                Mystery
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/books/Management" className="nav-link">
                Management
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/books" className="nav-link">
                All Books
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/book" className="nav-link">
                New Book
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default BookNavbar;
