import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
import http from "./bookServer";
import LeftPanel from "./leftPanel";
class ShowBooks extends Component {
  state = {
    data: {},
    languages: ["French", "English", "Latin", "Other"],
    bestSeller: ["Yes", "No"],
  };
  async fetchdata() {
    const { genre } = this.props.match.params;
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = genre
      ? await http.get(`/booksapp/books/${genre}?${searchStr}`)
      : await http.get(`/booksapp/books?${searchStr}`);
    let { data } = response;
    this.setState({ data: data });
    console.log(response);
  }
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchdata();
  }
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    let { genre } = this.props.match.params;
    genre
      ? this.callURL(`/books/${genre}`, queryParams)
      : this.callURL("/books", queryParams);
  };
  handleOptionChange = (options) => {
    let { genre } = this.props.match.params;
    genre
      ? this.callURL(`/books/${genre}`, options)
      : this.callURL("/books", options);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let { page, newarrival, language, bestseller } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    searchStr = this.addToQueryString(searchStr, "newarrival", newarrival);
    searchStr = this.addToQueryString(searchStr, "language", language);
    searchStr = this.addToQueryString(searchStr, "bestseller", bestseller);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { pageInfo = {}, books = [], refineOptions = {} } = this.state.data;
    let { bestSeller, languages } = this.state;
    let { pageNumber, numberOfPages, numOfItems, totalItemCount } = pageInfo;
    let queryParams = queryString.parse(this.props.location.search);
    let size = 10;
    let startIndex = queryParams.page ? (queryParams.page - 1) * size : 0;
    let endIndex = queryParams.page
      ? books.length < 10
        ? (queryParams.page - 1) * size + books.length
        : queryParams.page * size
      : numberOfPages === 1
      ? totalItemCount
      : 10;
    return (
      <div className="container">
        <div className="row">
          <div className="col-3 mt-3">
            <h5>Options</h5>
            <hr />
            <LeftPanel
              options={queryParams}
              bestSeller={bestSeller}
              languages={languages}
              refineOptions={refineOptions}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9 mt-3">
            <h5>
              {startIndex + 1} to {endIndex} of {totalItemCount}{" "}
            </h5>
            <div className="row bg-primary text-dark">
              <div className="col-3 border">Title</div>
              <div className="col-3 border">Author</div>
              <div className="col-2 border">Language</div>
              <div className="col-2 border">Genre</div>
              <div className="col-1 border">Price</div>
              <div className="col-1 border">BstSel</div>
            </div>
            {books.map((bk, index) => (
              <div className="row" key={index}>
                <div className="col-3 border">
                  <Link to={`/book/${bk.bookid}`}>{bk.name}</Link>
                </div>
                <div className="col-3 border">{bk.author}</div>
                <div className="col-2 border">{bk.language}</div>
                <div className="col-2 border">{bk.genre}</div>
                <div className="col-1 border">{bk.price}</div>
                <div className="col-1 border">{bk.bestseller}</div>
              </div>
            ))}
            <div className="row">
              <div className="col-2">
                {pageNumber > 1 ? (
                  <button
                    className="btn btn-primary m-1 btn-sm"
                    onClick={() => this.handlePage(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 offset-8 text-right">
                {pageNumber < numberOfPages ? (
                  <button
                    className="btn btn-primary btn-sm m-1"
                    onClick={() => this.handlePage(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ShowBooks;
