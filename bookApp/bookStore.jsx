import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import ShowBooks from "./showBooks";
import ShowBook from "./showBook";
import AddBook from "./addBook";
import BookNavbar from "./bookNavbar";
class SportStar extends Component {
  render() {
    return (
      <React.Fragment>
        <BookNavbar />
        <Switch>
          <Route path="/book/:id" component={ShowBook} />
          <Route path="/book" component={AddBook} />
          <Route path="/books/:genre" component={ShowBooks} />
          <Route path="/books" component={ShowBooks} />
          <Redirect from="/" to="/books" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default SportStar;
