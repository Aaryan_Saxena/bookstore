import React, { Component } from "react";
class LeftPanel extends Component {
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let options = { ...this.props.options };
    input.type === "checkbox"
      ? (options[input.name] = this.updateCBs(
          options[input.name],
          input.checked,
          input.value
        ))
      : (options[input.name] = input.value);
    options.page = "";
    this.props.onOptionChange(options);
  };
  updateCBs = (inpValue, checked, value) => {
    let inpArr = inpValue ? inpValue.split(",") : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((val) => val === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr.join(",");
  };
  makeRadio = (arr, values, name, label, options = []) => {
    return (
      <React.Fragment>
        <label className="form-check-label font-weight-bold">{label}</label>
        {arr.map((opt, index) => (
          <div className="form-check" key={opt}>
            <input
              type="radio"
              className="form-check-input"
              value={opt}
              name={name}
              checked={values === opt}
              onChange={this.handleChange}
            />
            <label className="form-check-label">
              {opt} ({options.length === 0 ? "" : options[index].totalNum})
            </label>
          </div>
        ))}
      </React.Fragment>
    );
  };
  makeCBs = (arr, values, name, label, options = []) => {
    return (
      <React.Fragment>
        <label className="form-check-label font-weight-bold">{label}</label>
        {arr.map((opt, index) => (
          <div className="form-check" key={opt}>
            <input
              type="checkbox"
              className="form-check-input"
              value={opt}
              name={name}
              checked={values.find((val) => val === opt) || false}
              onChange={this.handleChange}
            />

            <label className="form-check-label">
              {opt} ({options.length === 0 ? "" : options[index].totalNum})
            </label>
          </div>
        ))}
      </React.Fragment>
    );
  };
  render() {
    let { bestseller = "", language = "" } = this.props.options;
    let { languages, bestSeller, refineOptions = {} } = this.props;
    return (
      <div className="row">
        <div className="col-12">
          {this.makeRadio(
            bestSeller,
            bestseller,
            "bestseller",
            "BestSeller",
            refineOptions.bestseller
          )}
          <hr />
          {this.makeCBs(
            languages,
            language.split(","),
            "language",
            "Languages",
            refineOptions.language
          )}
        </div>
      </div>
    );
  }
}
export default LeftPanel;
