import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./bookServer";
class ShowBook extends Component {
  state = { book: {} };
  async componentDidMount() {
    let { id } = this.props.match.params;
    let response = await http.get(`/booksApp/book/${id}`);
    let { data } = response;
    console.log(response);
    this.setState({ book: data });
  }
  render() {
    let { book } = this.state;
    let {
      author,
      genre,
      description,
      blurb,
      review,
      price,
      avgrating,
      name,
    } = book;
    return (
      <div className="container">
        <h2 className="mt-4">Book : {name}</h2>
        <div className="row border border-secondary">
          <div className="col-3 text-muted">
            <h5>Author : </h5>
          </div>
          <div className="col-9 text-muted">
            <h5>{author}</h5>
          </div>
        </div>
        <div className="row border border-secondary">
          <div className="col-3 text-muted">
            <h5>Genre : </h5>
          </div>
          <div className="col-9 text-muted">
            <h5>{genre}</h5>
          </div>
        </div>
        <div className="row border border-secondary">
          <div className="col-3 text-muted">
            <h5>Description : </h5>
          </div>
          <div className="col-9 text-muted">
            <h5>{description}</h5>
          </div>
        </div>
        <div className="row border border-secondary">
          <div className="col-3 text-muted">
            <h5>Blurb : </h5>
          </div>
          <div className="col-9 text-muted">
            <h5>{blurb}</h5>
          </div>
        </div>
        <div className="row border border-secondary">
          <div className="col-3 text-muted">
            <h5>Review : </h5>
          </div>
          <div className="col-9 text-muted">
            <h5>{review}</h5>
          </div>
        </div>
        <div className="row border border-secondary">
          <div className="col-3 text-muted">
            <h5>Price : </h5>
          </div>
          <div className="col-9 text-muted">
            <h5>{price}</h5>
          </div>
        </div>
        <div className="row border border-secondary">
          <div className="col-3 text-muted">
            <h5>Rating : </h5>
          </div>
          <div className="col-9 text-muted">
            <h5>{avgrating}</h5>
          </div>
        </div>
      </div>
    );
  }
}
export default ShowBook;
